#! /bin/bash

FIND=/usr/bin/find
SORT=/usr/bin/sort
HEAD=/usr/bin/head
GREP=/usr/bin/grep
CAT=/usr/bin/cat
CUT=/usr/bin/cut
ECHO=/usr/bin/echo

NO_CACHE_HIT=no_cache_hit

SCRIPTPATH="$( cd "$(/usr/bin/dirname "$0")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/../theme.conf"

get_cache() {
    # args
    # 1: icon_name

    set -o pipefail

    RESULT=`($CAT "${cache_file}" | $GREP "${icon_theme}:${1}" | $CUT -d' ' -f2) 2>/dev/null || $ECHO $NO_CACHE_HIT`

    set +o pipefail
    $ECHO "${RESULT}"
}

set_cache() {
    # args
    # 1: icon_name
    # 2: icon_path

    $ECHO "${icon_theme}:${1} ${2}" >> ${cache_file}
}

icon_path() {
    # args
    # 1: icon_name

    RESULT=`get_cache "${1}"`
    if [[ "${RESULT}" == $NO_CACHE_HIT ]]; then
        RESULT=`$FIND -L "/usr/share/icons/${icon_theme}/" -name "*${1}*.svg" -type f | $GREP symbolic -v | $SORT -r | $HEAD -n 1`
        set_cache "${1}" "${RESULT}"
    fi

    echo "${RESULT}"
}

# allow for direct invocation
if [[ "${1}" == 'icon_path' ]]; then
    icon_path "${2}"
fi
