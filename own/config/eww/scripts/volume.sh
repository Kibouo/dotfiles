#! /bin/bash
PACTL=/usr/bin/pactl
HEAD=/usr/bin/head
EGREP=/usr/bin/egrep
ECHO=/usr/bin/echo
TR=/usr/bin/tr

SCRIPTPATH="$( cd "$(/usr/bin/dirname "${0}")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/utils.sh"

icon() {
    if [[ "${DEVICE}" == 'sink' ]]; then
        if [[ "${VOLUME}" == 0 || "${MUTED}" == 0 ]]; then
            icon_path 'player-volume-muted'
        else
            if [[ "${VOLUME}" -le 25 ]]; then
                icon_path 'audio-volume-low'
            elif [[ "${VOLUME}" -le 50 ]]; then
                icon_path 'audio-volume-medium'
            else
                icon_path 'audio-volume-high'
            fi
        fi

    elif [[ "${DEVICE}" == 'source' ]]; then
        if [[ "${VOLUME}" == 0 || "${MUTED}" == 0 ]]; then
            icon_path 'mic-volume-muted'
        else
            if [[ "${VOLUME}" -le 25 ]]; then
                icon_path 'mic-volume-low'
            elif [[ "${VOLUME}" -le 50 ]]; then
                icon_path 'mic-volume-medium'
            else
                icon_path 'mic-volume-high'
            fi
        fi

    else
        exit -1
    fi
}

action() {
    local NEW_VOLUME="${1}"

    # unmute 1st
    $PACTL "set-${DEVICE}-mute" `${PACTL} "get-default-${DEVICE}"` 0
    $PACTL "set-${DEVICE}-volume" `${PACTL} "get-default-${DEVICE}"` "${NEW_VOLUME}"
}


DEVICE="${2}"
VOLUME=`$PACTL "get-${DEVICE}-volume" $($PACTL "get-default-${DEVICE}") | $EGREP --only-matching '[0-9]+%' | $HEAD -1 | $TR -d \%`
$PACTL "get-${DEVICE}-mute" $($PACTL "get-default-${DEVICE}") | $EGREP 'yes' >/dev/null
MUTED=$?
if [[ "${1}" == 'icon' ]]; then
	icon
elif [[ "${1}" == 'perc' ]]; then
	$ECHO "${VOLUME}"
elif [[ "${1}" == 'action' ]]; then
    action "${3}"
fi