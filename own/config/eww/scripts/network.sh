#! /bin/bash

NMCLI=/usr/bin/nmcli
SYSTEMCTL=/usr/bin/systemctl
AWK=/usr/bin/awk
EGREP=/usr/bin/egrep


SCRIPTPATH="$( cd "$(/usr/bin/dirname "$0")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/utils.sh"


icon() {
    # disabled
    $SYSTEMCTL status NetworkManager >/dev/null
    if [[ $? -ne 0 ]]; then
        icon_path 'network-wireless-offline'
        return
    fi

    # connection type
    $NMCLI --fields TYPE,STATE device | $EGREP 'ethernet\s+connected' >/dev/null
    if [[ $? -eq 0 ]]; then
        icon_path 'network-wired'
        return
    fi

    local SIGNAL_STRENGTH=`$NMCLI -f IN-USE,SIGNAL device wifi | $AWK '/^\*/{if (NR!=1) {print $2}}'`
    if [[ $SIGNAL_STRENGTH -gt 95 ]]; then
        icon_path 'network-wireless-connected-100'
    elif [[ $SIGNAL_STRENGTH -gt 75 ]]; then
        icon_path 'network-wireless-connected-75'
    elif [[ $SIGNAL_STRENGTH -gt 50 ]]; then
        icon_path 'network-wireless-connected-50'
    elif [[ $SIGNAL_STRENGTH -gt 25 ]]; then
        icon_path 'network-wireless-connected-25'
    else
        icon_path 'network-wireless-connected-00'
    fi
}


if [[ "${1}" == 'icon' ]]; then
    icon
fi