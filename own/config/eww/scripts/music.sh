#! /bin/bash

set -e

PRINTF=/usr/bin/printf
PLAYERCTL=/usr/bin/playerctl

SEEK_DIFF_LIMIT=8

SCRIPTPATH="$( cd "$(/usr/bin/dirname "$0")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/utils.sh"


icon() {
    local SPECIFIER="${1}"

    if [[ "${SPECIFIER}" == 'main' ]]; then
        icon_path 'view-media-track'
    elif [[ "${SPECIFIER}" == 'toggle' ]]; then
        if [[ `$PLAYERCTL status` == 'Playing' ]]; then
            icon_path 'media-playback-pause'
        else
            icon_path 'media-playback-start'
        fi
    elif [[ "${SPECIFIER}" == 'next' ]]; then
        icon_path 'media-skip-forward'
    elif [[ "${SPECIFIER}" == 'previous' ]]; then
        icon_path 'media-skip-backward'

    else
        exit -1
    fi
}

round() {
    $PRINTF "%.0f\n" "${1}"
}

perc() {
    round `$PLAYERCTL metadata --format '{{ position * 100 / mpris:length }}'`
}

action() {
    local ACTION="${1}"

    if [[ "${ACTION}" == 'seek' ]]; then
        ACTION='position'
        local SEEK_PERC=`round $($PLAYERCTL metadata --format '{{ mpris:length / 1000000 * '"${2}"' / 100 }}')`

        # workaround for https://github.com/elkowar/eww/issues/482
        local PLAYER_PERC=`round $($PLAYERCTL metadata --format '{{ position / 1000000 }}')`
        local DIFF=$((SEEK_PERC - PLAYER_PERC))
        if [[ ${DIFF#-} -gt ${SEEK_DIFF_LIMIT} ]]; then
            $PLAYERCTL "${ACTION}" "${SEEK_PERC}"
        fi
    else
        if [[ "${ACTION}" == 'toggle' ]]; then
            ACTION='play-pause'
        fi
        $PLAYERCTL "${ACTION}"
    fi
}


if [[ "${1}" == 'icon' ]]; then
    icon "${2}"
elif [[ "${1}" == 'perc' ]]; then
    perc
elif [[ "${1}" == 'action' ]]; then
    action "${2}" "${3}"
fi
