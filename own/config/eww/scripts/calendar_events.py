#! /usr/bin/python3

import sqlite3
from datetime import datetime, timezone, timedelta, date
from dateutil import rrule
from icalendar import Calendar, prop
from tzlocal import get_localzone
import os
import sys


def date_to_datetime(val: date) -> datetime:
    return datetime.combine(val, datetime.min.time())


def datetime_to_ical_datetimestring(val: datetime) -> str:
    return prop.vDatetime(val).to_ical().decode('utf-8')


NOW: datetime = datetime.now(timezone.utc)
NEARBY_FUTURE_LIMIT: datetime = NOW + timedelta(days=1)


def build_gnome_calendar_event_uuid(source_id: str, event_id: str, recurrency_id: str = None) -> str:
    # gnome-calendar can be started with the `--uuid` flag to auto-focus on an event. This uuid is formatted as '[source id]:[event id]:[recurrency id]'. These are the following items:
    # - *source id*: each synced calendar's settings are stored under `~/.cache/evolution/calendar/` in a dir with a random string as name. The `source id` is this dir's name.
    # - *event id*: the UID of the calendar event in question. Can be found in its ICS entry.
    # - *recurrency id*: only applicable in recurring events. Recurrency is marked with the `RRULE:<...>` property. The `recurrency id` is the `DTSTART` of the specific event, i.e. date of the recurring instance occurring.
    #
    # gnome-calendar references:
    # - build uid: https://gitlab.gnome.org/GNOME/gnome-calendar/-/blob/main/src/core/gcal-event.c#L255
    # - describe uid: https://gitlab.gnome.org/GNOME/gnome-calendar/-/blob/main/src/core/gcal-event.c#L1449
    uuid = f'{source_id}:{event_id}'
    if recurrency_id is not None:
        uuid += f':{recurrency_id}'
    return uuid


def next_near_future_recurrence(rule: prop.vRecur, start: datetime) -> list:
    rule = rule.to_ical().decode('utf-8')
    if 'UNTIL' not in rule:
        rule += f';UNTIL={datetime_to_ical_datetimestring(NEARBY_FUTURE_LIMIT)}'

    rule_set = rrule.rrulestr(
        rule,
        dtstart=start,
        cache=True,
        unfold=True,
        forceset=True,
        ignoretz=False
    )

    # add 1min to skip events that would be "in 0minutes (x secs)", as that looks and is silly to notify us of.
    return rule_set.after(NOW + timedelta(minutes=1))


def sources() -> filter:
    return filter(
        lambda x: len(x) == 40,
        os.listdir(f'{os.environ.get("HOME")}/.cache/evolution/calendar/')
    )


class Event():
    def __init__(self, summary: str, start: datetime, gnome_calendar_uuid: str):
        self.summary = summary
        self.start = start
        self.uuid = gnome_calendar_uuid


def source_to_next_events(source: str) -> list:
    events = []

    connection = sqlite3.connect(
        f'{os.environ.get("HOME")}/.cache/evolution/calendar/{source}/cache.db')

    cursor = connection.cursor()
    for row in cursor.execute('SELECT ECacheOBJ FROM ECacheObjects'):
        calendar = Calendar.from_ical(row[0])
        for component in calendar.walk():
            if component.name == "VEVENT":
                summary = component.get('summary')
                uid = component.get('uid')
                dtstart = component.get('dtstart')
                recurrence_rule = component.get('rrule')

                datetime_start = prop.vDDDTypes.from_ical(dtstart)
                if type(datetime_start) == date:
                    datetime_start = date_to_datetime(datetime_start)
                    datetime_start = datetime_start.astimezone(get_localzone())

                if recurrence_rule is None:
                    # if no recurrence, manually check if start datetime is in the near future
                    if datetime_start > NOW and datetime_start < NEARBY_FUTURE_LIMIT:
                        events.append(Event(
                            summary,
                            datetime_start,
                            build_gnome_calendar_event_uuid(source, uid)
                        ))
                else:
                    # else use recurrency rules to find next starting datetime in the near future
                    try:
                        upcoming_start_datetime = next_near_future_recurrence(
                            recurrence_rule, datetime_start)
                    except:
                        upcoming_start_datetime = None
                    if upcoming_start_datetime is not None:
                        events.append(Event(
                            summary,
                            upcoming_start_datetime,
                            build_gnome_calendar_event_uuid(
                                source,
                                uid,
                                datetime_to_ical_datetimestring(
                                    upcoming_start_datetime)
                            )
                        ))
    connection.close()

    return events


def human_readable_time_till(val: datetime) -> str:
    diff = val - NOW
    hours, remainder = divmod(int(diff.total_seconds()), 60 * 60)
    mins, _secs = divmod(remainder, 60)

    if hours > 0:
        return f'In {hours:02d}h [{val.strftime("%H:%M")}]'
    else:
        return f'In {mins:02d}m [{val.strftime("%H:%M")}]'


CLI_FLAGS = ['info', 'uuid']


def arg_parse() -> str:
    if len(sys.argv) == 2 and sys.argv[1] in CLI_FLAGS:
        return sys.argv[1]
    else:
        print(f'ERROR: required one of {CLI_FLAGS}', file=sys.stderr)
        exit(-1)


def main():
    flag = arg_parse()

    next_events = []
    for source in sources():
        next_events += source_to_next_events(source)
    next_events.sort(key=lambda e: e.start)

    event = next_events[0] if len(next_events) > 0 else None
    if flag == CLI_FLAGS[0]:
        if event is not None:
            print(f'{human_readable_time_till(event.start)}: {event.summary}')
        else:
            print('No events planned')

    elif flag == CLI_FLAGS[1]:
        if event is not None:
            print(event.uuid)
        else:
            print('InvalidUUID')

    else:
        print(f'ERROR: {flag} not implemented!', file=sys.stderr)


main()
