#! /bin/bash

DUNSTCTL=/usr/bin/dunstctl


SCRIPTPATH="$( cd "$(/usr/bin/dirname "$0")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/utils.sh"


icon() {
    if [[ `$DUNSTCTL is-paused` == 'false' ]]; then
        icon_path 'notifications'
    else
        icon_path 'notification-disabled'
    fi
}


if [[ "${1}" == 'icon' ]]; then
    icon
fi