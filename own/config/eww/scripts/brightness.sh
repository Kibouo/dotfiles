#! /bin/bash

BRIGHTNESSCTL=/usr/bin/brightnessctl
TR=/usr/bin/tr
CUT=/usr/bin/cut

SCRIPTPATH="$( cd "$(/usr/bin/dirname "$0")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/utils.sh"


if [[ "${1}" == 'icon' ]]; then
	icon_path 'preferences-system-brightness-lock'
elif [[ "${1}" == 'perc' ]]; then
	$BRIGHTNESSCTL -m info | $CUT -d, -f 4 | $TR -d \%
fi