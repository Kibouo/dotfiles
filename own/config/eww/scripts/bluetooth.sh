#! /bin/bash

SYSTEMCTL=/usr/bin/systemctl
BLUETOOTHCTL=/usr/bin/bluetoothctl
CUT=/usr/bin/cut
GREP=/usr/bin/grep
ECHO=/usr/bin/echo


SCRIPTPATH="$( cd "$(/usr/bin/dirname "$0")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/utils.sh"

any_connected_device() {
    $BLUETOOTHCTL paired-devices | $CUT -f2 -d' '| while read -r uuid; do
        info=`$BLUETOOTHCTL info $uuid`
        if $ECHO "$info" | $GREP -q 'Connected: yes'; then
            $ECHO "$info" | $GREP 'Name'
            break
        fi
    done
}

icon() {
    # disabled
    $SYSTEMCTL status bluetooth >/dev/null
    if [[ $? -ne 0 ]]; then
        icon_path 'bluetooth-disabled'
        return
    fi

    # connected
    any_connected_device | $GREP 'Name' >/dev/null
    if [[ $? -eq 0 ]]; then
        icon_path 'bluetooth-paired'
        return
    fi

    # on but not connected
    icon_path 'bluetooth'
}


if [[ "${1}" == 'icon' ]]; then
    icon
fi