#! /bin/bash

ACPI=/usr/bin/acpi
CUT=/usr/bin/cut
TR=/usr/bin/tr
ECHO=/usr/bin/echo

SCRIPTPATH="$( cd "$(/usr/bin/dirname "$0")" ; /usr/bin/pwd -P )"
source "${SCRIPTPATH}/utils.sh"

icon() {
    local BATTERY="${1}"

	if [[ `$ACPI | $CUT -d' ' -f3 | $TR -d \,` == 'Charging' ]]; then
		local CHARGING='-charg'
    fi

    if [[ "${BATTERY}" -gt 95 ]]; then
        icon_path "battery-full${CHARGING}"
    elif [[ "${BATTERY}" -gt 75 ]]; then
        icon_path "battery-good${CHARGING}"
    elif [[ "${BATTERY}" -gt 50 ]]; then
        icon_path "battery-medium${CHARGING}"
    elif [[ "${BATTERY}" -gt 15 ]]; then
        icon_path "battery-low${CHARGING}"
    elif [[ "${BATTERY}" -gt 0 ]]; then
        icon_path "battery-caution${CHARGING}"
	else
        icon_path 'battery-missing'
    fi
}


PERC=`$ACPI | $CUT -d' ' -f4 | $TR -d \%\,`
if [[ "${1}" == 'icon' ]]; then
	icon "${PERC}"
elif [[ "${1}" == 'perc' ]]; then
	$ECHO "${PERC}"
fi