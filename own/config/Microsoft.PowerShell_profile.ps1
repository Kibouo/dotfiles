$ENV:HOME = "C:/Users/${ENV:USERNAME}"
$ENV:STARSHIP_CONFIG = "${HOME}/.config/starship.toml"
$ENV:PATH += "D:/Documents/windows_scripts/;${ENV:APPDATA}/../Local/Android/Sdk/emulator/;${ENV:PROGRAMFILES}/LLVM/bin/;${ENV:PROGRAMDATA}/chocolatey/bin/"
$CHOCO_BINS = "${ENV:PROGRAMDATA}/chocolatey/bin"

Invoke-Expression (& "${ENV:PROGRAMFILES}/starship/bin/starship.exe" init powershell )
Invoke-Expression (& { (iex "${CHOCO_BINS}/zoxide.exe init --cmd cd powershell" | Out-String) })

function find { iex "${CHOCO_BINS}/fd.exe -H ${args}" }
function cat_replaced { iex "${CHOCO_BINS}/bat.exe --theme=ansi --paging=never ${args}" }
Set-Alias -Name cat -Force -Value cat_replaced -Option AllScope
function rcat { iex "${CHOCO_BINS}/bat.exe --theme=ansi --paging=never --plain ${args}" }
function ls_replaced {
  if ($args -contains 'l') {
    $args += ' --blocks permission,size,user,group,date,name'
  }
  iex "${CHOCO_BINS}/lsd.exe --group-directories-first --date '+%d %b %H:%M' --header --icon never --size short ${args}"
}
Set-Alias -Name ls -Force -Value ls_replaced -Option AllScope
function grep { iex "${CHOCO_BINS}/rga.exe --smart-case --multiline -e ${args}" }
function sed { iex "${CHOCO_BINS}/sd.exe -f i ${args}" }
function diff_replaced { iex "${CHOCO_BINS}/difft.exe --display=side-by-side-show-both ${args}" }
Set-Alias -Name diff -Force -Value diff_replaced -Option AllScope
function which { (Get-Command $args).source }
function python3 { iex "python.exe ${args}" }

function run_android_emu {
  Param(
    [Int]$proxy_index
  )
  Start-Process emulator -ArgumentList '-avd', "Default_${proxy_index}", '-http-proxy', "127.0.0.1:$(8080 + $proxy_index)", '-writable-system'
}

function wsl_socks {
  Param(
    [Int]$socks_port
  )
  iex "ssh -D 127.0.0.1:${socks_port} kibouo@172.20.106.193"
}

# Fix keybinds
Set-PSReadlineKeyHandler -Key ctrl+d -Function ViExit

# chocolatey auto-complete
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}

# intellisense
Set-PSReadLineOption -PredictionSource History
