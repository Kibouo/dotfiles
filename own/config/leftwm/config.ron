//  _        ___                                      ___ _
// | |      / __)_                                   / __|_)
// | | ____| |__| |_ _ _ _ ____      ____ ___  ____ | |__ _  ____    ____ ___  ____
// | |/ _  )  __)  _) | | |    \    / ___) _ \|  _ \|  __) |/ _  |  / ___) _ \|  _ \
// | ( (/ /| |  | |_| | | | | | |  ( (__| |_| | | | | |  | ( ( | |_| |  | |_| | | | |
// |_|\____)_|   \___)____|_|_|_|   \____)___/|_| |_|_|  |_|\_|| (_)_|   \___/|_| |_|
// A WindowManager for Adventurers                         (____/
// For info about configuration please visit https://github.com/leftwm/leftwm/wiki

#![enable(implicit_some)]
(
    modkey: "Alt",
    mousekey: "Alt",
    workspaces: [],
    tags: [
        "一",
        "二",
        "三",
        "四",
        "五",
        "六",
        "七",
        "八",
        "九",
    ],
    max_window_width: None,
    layouts: [
        "Grid",
        "MainAndVertStack",
        "Monocle",
    ],
    layout_mode: Tag,
    insert_behavior: Bottom,
    scratchpad: [],
    window_rules: [
        (window_class: None, window_title: "Picture-in-Picture", spawn_on_tag: None, spawn_floating: true),
        (window_class: None, window_title: "Ghidra: ghidra", spawn_on_tag: None, spawn_floating: true),
    ],
    disable_current_tag_swap: true,
    disable_tile_drag: true,
    disable_window_snap: true,
    focus_behaviour: Sloppy,
    focus_new_windows: true,
    sloppy_mouse_follows_focus: true,
    keybind: [
        (command: Execute, value: "/usr/bin/rofi -show combi", modifier: ["modkey"], key: "p"),
        (command: Execute, value: "__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia __VK_LAYER_NV_optimus=NVIDIA_only VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/nvidia_icd.json /sbin/alacritty --working-directory=${HOME}/Downloads", modifier: ["Control", "modkey"], key: "backslash"),
        (command: CloseWindow, value: "", modifier: ["modkey"], key: "q"),
        (command: Execute, value: "${HOME}/.local/bin/change_volume.sh mute sink toggle", modifier: [], key: "XF86XK_AudioMute"),
        (command: Execute, value: "${HOME}/.local/bin/change_volume.sh volume sink -3%", modifier: [], key: "XF86XK_AudioLowerVolume"),
        (command: Execute, value: "${HOME}/.local/bin/change_volume.sh volume sink +3%", modifier: [], key: "XF86XK_AudioRaiseVolume"),
        (command: Execute, value: "${HOME}/.local/bin/change_volume.sh mute source toggle", modifier: [], key: "XF86XK_AudioMicMute"),
        (command: Execute, value: "${HOME}/.local/bin/change_brightness.sh \'5%-\'", modifier: [], key: "XF86XK_MonBrightnessDown"),
        (command: Execute, value: "${HOME}/.local/bin/change_brightness.sh \'5%+\'", modifier: [], key: "XF86XK_MonBrightnessUp"),
        (command: FocusPreviousTag, value: "", modifier: ["Control", "modkey"], key: "Left"),
        (command: FocusNextTag, value: "", modifier: ["Control", "modkey"], key: "Right"),
        (command: MoveWindowToPreviousTag, value: "true", modifier: ["Shift", "modkey"], key: "Left"),
        (command: MoveWindowToNextTag, value: "true", modifier: ["Shift", "modkey"], key: "Right"),
        (command: MoveWindowUp, value: "", modifier: ["Shift", "modkey"], key: "Up"),
        (command: MoveWindowDown, value: "", modifier: ["Shift", "modkey"], key: "Down"),
        (command: FocusWindowDown, value: "", modifier: ["Alt"], key: "Tab"),
        (command: FocusWindowUp, value: "", modifier: ["modkey"], key: "Up"),
        (command: FocusWindowDown, value: "", modifier: ["modkey"], key: "Down"),
        (command: FocusWorkspaceNext, value: "", modifier: ["Super", "modkey"], key: "Right"),
        (command: FocusWorkspacePrevious, value: "", modifier: ["Super", "modkey"], key: "Left"),
        (command: ToggleFullScreen, value: "", modifier: ["modkey"], key: "f"),
        (command: Execute, value: "${HOME}/.local/bin/screenshot.sh screen file", modifier: ["Super"], key: "Print"),
        (command: Execute, value: "${HOME}/.local/bin/screenshot.sh screen clip", modifier: [], key: "Print"),
        (command: Execute, value: "${HOME}/.local/bin/screenshot.sh window file", modifier: ["Super", "Shift"], key: "Print"),
        (command: Execute, value: "${HOME}/.local/bin/screenshot.sh window clip", modifier: ["Shift"], key: "Print"),
        (command: Execute, value: "${HOME}/.local/bin/screenshot.sh area file", modifier: ["Super", "Alt"], key: "Print"),
        (command: Execute, value: "${HOME}/.local/bin/screenshot.sh area clip", modifier: ["Alt"], key: "Print"),
        (command: SoftReload, value: "", modifier: ["modkey", "Shift"], key: "r"),
        (command: NextLayout, value: "", modifier: ["modkey"], key: "space"),
        (command: IncreaseMainWidth, value: "4", modifier: ["modkey"], key: "period"),
        (command: DecreaseMainWidth, value: "4", modifier: ["modkey"], key: "comma"),
        (command: Execute, value: "/usr/bin/dunstctl set-paused toggle", modifier: ["modkey"], key: "m"),
        (command: Execute, value: "${HOME}/.local/bin/rofi-dunst-notif-center.py", modifier: ["modkey"], key: "h"),
        (command: ToggleFloating, value: "", modifier: ["modkey", "Shift"], key: "f"),
        (command: Execute, value: "/usr/bin/loginctl lock-session", modifier: ["Super"], key: "l"),
    ],
    state_path: None,
)
