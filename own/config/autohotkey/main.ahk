;ignore OfficeKey combination
+!^#::
#^!Shift::
#^+Alt::
#!+Ctrl::
Send {Blind}{vkE8}
Return

;quit app
!q::
Send {Blind}{vkE8}
Send !{F4}
return

;fullscreen
!f::
Send {Blind}{vkE8}
Send {F11}
return

;reload desktop management stuff
+!r::
Send {Blind}{vkE8}
Run, powershell %userprofile%/.config/autohotkey/reload_desktop.ps1
Reload ;reload ahk itself
return

;run admin shell
+!\::
Send {Blind}{vkE8}
try {
    EnvGet, APPDATA, ENV:APPDATA
    Run, "%APPDATA%/Microsoft/Windows/Start Menu/Programs/Terminal - Admin.lnk"
} catch e {
}
return

;run user shell
^!\::
Send {Blind}{vkE8}
EnvGet, APPDATA, ENV:APPDATA
Run, "%APPDATA%/Microsoft/Windows/Start Menu/Programs/Terminal.lnk"
return

;cycle tiled app up
+!Up::
Send {Blind}{vkE8}
;CycleMove("previous")
return

;cycle tiled app down
+!Down::
Send {Blind}{vkE8}
;CycleMove("next")
return

;make window smaller
!,::
Send {Blind}{vkE8}
;ResizeAxis("horizontal", "decrease")
return

;make window bigger
!.::
Send {Blind}{vkE8}
;ResizeAxis("horizontal", "increase")
return

;goto left desktop
^!Left::
Send {Blind}{vkE8}
Send ^#{Left}
; restore focus on window
Send !{Esc}
Send +!{Esc}
; allow continued cycling
Send {LWin}
Send {LWin}
return

;goto right desktop
^!Right::
Send {Blind}{vkE8}
Send ^#{Right}
; restore focus on window
Send !{Esc}
Send +!{Esc}
; allow continued cycling
Send {LWin}
Send {LWin}
return

;app to right desktop
+!Right::
Send {Blind}{vkE8}
; TODO
return

;app to left desktop
+!Left::
Send {Blind}{vkE8}
; TODO
return

;prev app
!Up::
Send {Blind}{vkE8}
Send !{Esc}
return

;next app
!Down::
Send {Blind}{vkE8}
Send +!{Esc}
return

;app overview
^!Up::
Send {Blind}{vkE8}
Send #{Tab}
return

;app overview
^!Down::
Send {Blind}{vkE8}
Send #{Tab}
return

;start tiling
!Space::
Send {Blind}{vkE8}
Send #z
Send {# up}
return

;open snipping tool
!PrintScreen::
Send {Blind}{vkE8}
Send +#{PrintScreen}
return

;advanced paste
!v::
Send {Blind}{vkE8}
Send #{v}
return