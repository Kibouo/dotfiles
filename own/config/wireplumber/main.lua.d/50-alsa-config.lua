alsa_monitor.rules = {
    {
        apply_properties = {
            -- Use ALSA-Card-Profile devices. They use UCM or the profile
            -- configuration to configure the device and mixer settings.
            -- ["api.alsa.use-acp"] = true,
 
            -- Use UCM instead of profile when available. Can be
            -- disabled to skip trying to use the UCM profile.
            ["api.alsa.use-ucm"] = true,
        }
    }
}
