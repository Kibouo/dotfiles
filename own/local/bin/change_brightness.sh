#! /bin/bash

set -e

# params
VALUE="${1}"  # perc to incr./decr.

# vars
DUNST_TAG="brightness_notif"
DUNST_APP_NAME="change_brightness"

# bins (prevent path poisoning)
BRIGHTNESSCTL=/usr/bin/brightnessctl
DUNSTIFY=/usr/bin/dunstify
TR=/usr/bin/tr
CUT=/usr/bin/cut
ECHO=/usr/bin/echo

# update
$BRIGHTNESSCTL set "${VALUE}" --min-value 128

# notif
BRIGHTNESS=`$BRIGHTNESSCTL -m info | $CUT -d, -f 4 | $TR -d \%`

icon() {
    $ECHO 'preferences-system-brightness-lock'
}

$DUNSTIFY -a "${DUNST_APP_NAME}" -u low -i `icon` \
    -h "string:x-dunst-stack-tag:${DUNST_TAG}" \
    -h "int:value:${BRIGHTNESS}" 'Brightness' "${BRIGHTNESS}%"
