#! /bin/bash

set -e

# bin full paths
STDBUF=/usr/bin/stdbuf
GREP=/usr/bin/grep
AUTORANDR=/usr/bin/autorandr

# monitors
$STDBUF -oL /usr/bin/libinput debug-events | $GREP -E --line-buffered '^[[:space:]-]+event[0-9]+[[:space:]]+SWITCH_TOGGLE[[:space:]]' | while read line; do $AUTORANDR --change --default default; done
