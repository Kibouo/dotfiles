#! /usr/bin/python3

import subprocess
import json
import os
from datetime import datetime, timedelta

DUNSTCTL='/usr/bin/dunstctl'
ROFI='/usr/bin/rofi'
ROFI_NAME='Notifications'

def exec_shell(binary: str, args: list = [], input: str = None, env_vars: dict = {}) -> str:
    return subprocess.run([binary] + args,
        input=input, env=env_vars, capture_output=True, check=True
    ).stdout

def history():
    try:
        return exec_shell(
            DUNSTCTL, args=['history'],
            env_vars={'DBUS_SESSION_BUS_ADDRESS': os.environ.get('DBUS_SESSION_BUS_ADDRESS')}
        )
    except subprocess.CalledProcessError:
        return '{}'

def system_uptime_secs():
    with open('/proc/uptime', 'r') as uptime:
        return int(uptime.readline().split('.')[0])

def history_to_rofi(notifications: list) -> list:
    def history_entry_to_rofi_entry(notif: dict):
        icon = notif['icon_path']['data']
        icon = icon if icon != '' else 'notifications'
        age = system_uptime_secs() - notif['timestamp']['data']/1000000
        timestamp = datetime.now() - timedelta(seconds=age)
        summary = notif['summary']['data']
        body = notif['body']['data']

        return f'[{timestamp.strftime("%H:%M")}] {summary}: {body}\0icon\x1f{icon}'.replace("\n", ' ')

    return list(map(lambda e: history_entry_to_rofi_entry(e), notifications))


def display_rofi(rofi_entries: list):
    exec_shell(
        ROFI, args=['-dmenu', '-p', ROFI_NAME],
        input='\n'.join(rofi_entries).encode('utf-8'),
        env_vars={'DISPLAY':os.environ.get('DISPLAY')}
    )


def main():
    parsed_history = json.loads(history())
    rofi_entries = history_to_rofi(parsed_history['data'][0])
    display_rofi(rofi_entries)

main()
