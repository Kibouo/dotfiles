#! /bin/bash

set -e

while [[ 1 ]]
do
    /usr/bin/feh --randomize --no-fehbg --bg-fill ~/GoogleDrive/Pictures/Wallpapers/
    /usr/bin/sleep 180
done
