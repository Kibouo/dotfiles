#! /bin/bash

FIND=/usr/bin/fd
RM=/usr/bin/rm
FFMPEG=/usr/bin/ffmpeg
MV=/usr/bin/mv
SED=/usr/bin/sd
ECHO=/usr/bin/echo
BASENAME=/usr/bin/basename
DIRNAME=/usr/bin/dirname

$FIND -H --type f --extension flac --exec $FFMPEG -i {} -ab 160k -map_metadata 0 -id3v2_version 3 {.}.mp3
$FIND -H --type f --extension flac --exec $RM {}

# remove index
IFS=$'\n'
for f in `"$FIND" -H --type f --extension mp3`; do
    PATH=`$DIRNAME "$f"`
    BASE=`$BASENAME "$f"`
    NEW_BASE=`$ECHO "$BASE" | $SED -f i '^\d+( |\.|-)*' ''`

    $MV "$f" "$PATH/$NEW_BASE" >/dev/null
done

# move files in subdirs to cur dir
$FIND --type f --extension .mp3 --exec $MV {} .
$FIND --type d --max-depth 1 --exec $RM -rf {}
