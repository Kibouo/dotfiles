#! /bin/bash

set -e

# params
ACTION="${1}" # mute or volume
DEVICE="${2}" # sink or source
VALUE="${3}"  # depends on action

# vars
DUNST_TAG="volume_notif"
DUNST_APP_NAME="change_volume"

# bins (prevent path poisoning)
PACTL=/usr/bin/pactl
SOUND_PLAY=/usr/bin/canberra-gtk-play
EGREP=/usr/bin/egrep
HEAD=/usr/bin/head
DUNSTIFY=/usr/bin/dunstify
ECHO=/usr/bin/echo
TR=/usr/bin/tr

# update
if [[ "${ACTION}" == 'volume' ]]; then
    $PACTL "set-${DEVICE}-mute" `${PACTL} "get-default-${DEVICE}"` 0
fi
$PACTL "set-${DEVICE}-${ACTION}" `${PACTL} "get-default-${DEVICE}"` "${VALUE}"

# sound
$SOUND_PLAY -i audio-volume-change -d "change_volume"

# notif
VOLUME=`$PACTL "get-${DEVICE}-volume" $($PACTL "get-default-${DEVICE}") | $EGREP --only-matching '[0-9]+%' | $HEAD -1 | $TR -d \%`
set +e
$PACTL "get-${DEVICE}-mute" $($PACTL "get-default-${DEVICE}") | $EGREP 'yes' >/dev/null
MUTED=$?
set -e

icon() {
    if [[ "${DEVICE}" == 'sink' ]]; then
        if [[ "${VOLUME}" == 0 || "${MUTED}" == 0 ]]; then
            $ECHO 'player-volume-muted'
        else
            if [[ "${VOLUME}" -le 25 ]]; then
                $ECHO 'audio-volume-low'
            elif [[ "${VOLUME}" -le 50 ]]; then
                $ECHO 'audio-volume-medium'
            else
                $ECHO 'audio-volume-high'
            fi
        fi

    elif [[ "${DEVICE}" == 'source' ]]; then
        if [[ "${VOLUME}" == 0 || "${MUTED}" == 0 ]]; then
            $ECHO 'mic-volume-muted'
        else
            if [[ "${VOLUME}" -le 25 ]]; then
                $ECHO 'mic-volume-low'
            elif [[ "${VOLUME}" -le 50 ]]; then
                $ECHO 'mic-volume-medium'
            else
                $ECHO 'mic-volume-high'
            fi
        fi

    else
        exit -1
    fi
}

if [[ "${VOLUME}" == 0 || "${MUTED}" == 0 ]]; then
    $DUNSTIFY -a "${DUNST_APP_NAME}" -u low -i `icon` \
        -h "string:x-dunst-stack-tag:${DUNST_TAG}" \
        -h int:value:0 'Volume' 'muted'
else
    $DUNSTIFY -a "${DUNST_APP_NAME}" -u low -i `icon` \
        -h "string:x-dunst-stack-tag:${DUNST_TAG}" \
        -h "int:value:${VOLUME}" 'Volume' "${VOLUME}%"
fi
