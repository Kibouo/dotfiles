#! /bin/bash

set -e

# params
METHOD="${1}"    # screen, window, area
SAVE_TYPE="${2}" # file or clip

# vars
DUNST_TAG="screenshot_notif"
DUNST_APP_NAME="take_screenshot"

# bins (prevent path poisoning)
MAIM=/usr/bin/maim
XCLIP=/usr/bin/xclip
XDOTOOL=/usr/bin/xdotool
SOUND_PLAY=/usr/bin/canberra-gtk-play
DUNSTIFY=/usr/bin/dunstify
DATE=/usr/bin/date
ECHO=/usr/bin/echo
MKDIR=/usr/bin/mkdir

# make screenshot
log_err() { >&2 $ECHO -e "\033[1;31mError: $@"; }

case "${METHOD}" in
'screen')
    $MAIM --hidecursor | $XCLIP -selection buffer-cut -t image/png
    ;;
'window')
    $MAIM --hidecursor --window=`$XDOTOOL getactivewindow` | $XCLIP -selection buffer-cut -t image/png
    ;;
'area')
    $MAIM --hidecursor --select | $XCLIP -selection buffer-cut -t image/png
    ;;
*)
    log_err 'Invalid screenshot method!'
    exit 1
esac

if [[ "${SAVE_TYPE}" == 'file' ]]; then
    $MKDIR -p "${HOME}/Pictures/Screenshots/"
    screenshot_file="${HOME}/Pictures/Screenshots/screenshot_`${DATE} +%Y-%m-%d_%H:%M:%S`.png"
    $XCLIP -o -selection buffer-cut > "${screenshot_file}"

    $DUNSTIFY -a "${DUNST_APP_NAME}" -u low -i photo \
        -h "string:x-dunst-stack-tag:${DUNST_TAG}" 'Screenshot' "file://${screenshot_file}"
else
    $XCLIP -o -selection buffer-cut | $XCLIP -selection clipboard -t image/png

    $DUNSTIFY -a "${DUNST_APP_NAME}" -u low -i photo \
        -h "string:x-dunst-stack-tag:${DUNST_TAG}" 'Screenshot' 'clipboard'
fi

# sound
$SOUND_PLAY -i camera-shutter -d 'make_screenshot'
