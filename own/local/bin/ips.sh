#! /sbin/bash

DIG=/sbin/dig
PRINTF=/sbin/printf
IP=/sbin/ip
GREP=/sbin/grep
SED=/sbin/sd
ECHO=/sbin/echo
CUT=/sbin/cut
COLUMN=/sbin/column

COLUMN_FORMAT="%-16s\t%-16s\n"
_public () {
    local IPv4=`$DIG +short myip.opendns.com A @resolver4.opendns.com`
    if [[ ${IPv4} ]]; then $PRINTF ${COLUMN_FORMAT} 'Public IPv4:' ${IPv4}; fi

    # `local` is a command and would overwrite the return code of `dig`, so we drop it here
    IPv6=`$DIG +short -6 myip.opendns.com AAAA @resolver1.ipv6-sandbox.opendns.com`
    if [[ ${IPv6} && $? == 0 ]]; then $PRINTF ${COLUMN_FORMAT} 'Public IPv6:' ${IPv6}; fi
}

_private () {
    local IFS=$'\n'

    for IF in `$IP address | $GREP 'inet ' | $GREP 'scope global'`; do
      $PRINTF ${COLUMN_FORMAT} `$ECHO ${IF} | $SED '.* ' ''`':' `$ECHO ${IF} | $CUT -d' ' -f6 | $CUT -d'/' -f1`
    done
}

_private | $COLUMN -t -s $'\t'
_public | $COLUMN -t -s $'\t'
