#! /bin/bash

MIMEOPEN=/usr/bin/vendor_perl/mimeopen
WSL_OPEN=/usr/sbin/wsl-open

$WSL_OPEN "${@}" || $MIMEOPEN -n -M "${@}" &>/dev/null
