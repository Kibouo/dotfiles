#! /bin/bash

cd ./own/
    dotter deploy -f
cd - >/dev/null

# ensure externs are updated
git submodule update --init --recursive

link() {
    unlink "${2}"
    ln -f -s "${1}" "${2}"
}

# choose catppuccin colour scheme
PS3='Choose catppuccin colour scheme [dark --> light]: '
colours=("mocha" "macchiato" "frappe" "latte")
select colour in "${colours[@]}"
do
    if [[ "${colour}" == '' ]]; then
        echo "ERROR: Not a number" >&2
    else
        cd ./extern/
            link `pwd`/alacritty_theme/catppuccin-${colour}.yml ~/.config/alacritty/theme.yml

            link `pwd`/xresources/${colour}.Xresources ~/.Xresources.d/theme
        cd - >/dev/null
        break
    fi
done

# setup hideit
cd ./extern/
    link `pwd`/hideIt.sh/hideIt.sh ~/.local/bin/hideIt.sh
cd - >/dev/null

# setup gef-extras
cd ./extern/
    GEF_EXTRAS=~/.config/gef-extras
    link `pwd`/gef-extras "${GEF_EXTRAS}"
    python -m pip install --requirement "${GEF_EXTRAS}"/requirements.txt --upgrade
    gdb -q -ex "gef config gef.extra_plugins_dir '${GEF_EXTRAS}/scripts'" \
        -ex "gef config pcustom.struct_path '${GEF_EXTRAS}/structs'" \
        -ex "gef config syscall-args.path '${GEF_EXTRAS}/syscall-tables'" \
        -ex "gef config context.libc_args True" \
        -ex "gef config context.libc_args_path '${GEF_EXTRAS}/glibc-function-args'" \
        -ex 'gef save' \
        -ex quit
cd - >/dev/null
# gef-extras keeps applying already existing modifications. Trash those
git checkout HEAD -- ./own/gef_rc
