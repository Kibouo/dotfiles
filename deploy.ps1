function mklink ($link, $target) {
    New-Item -Path $link -ItemType SymbolicLink -Value $target -Force
}

mklink "${HOME}/.gitconfig" "${PSScriptRoot}/own/windows_gitconfig"
mklink "${HOME}/.gitignore" "${PSScriptRoot}/own/gitignore"
mklink "${HOME}/.clang-format" "${PSScriptRoot}/own/clang-format"
mklink "${HOME}/.wslconfig" "${PSScriptRoot}/own/wslconfig"
mklink "${HOME}/.config/" "${PSScriptRoot}/own/config/"

mklink "${HOME}/Documents/WindowsPowerShell/Microsoft.PowerShell_profile.ps1" "${PSScriptRoot}/own/config/Microsoft.PowerShell_profile.ps1"

mklink "${HOME}/Documents/PowerToys/" "${PSScriptRoot}/own/config/PowerToys/"

$wt_publisher_id = (Get-AppxPackage Microsoft.WindowsTerminal).PublisherId
mklink "${ENV:APPDATA}/../Local/Packages/Microsoft.WindowsTerminal_${wt_publisher_id}/LocalState/settings.json" "${PSScriptRoot}/own/config/wt/LocalState/settings.json"

mklink "${ENV:PROGRAMDATA}/chocolatey/lib/x64dbg.portable/tools/release/x64/x64dbg.ini" "${PSScriptRoot}/own/config/x64dbg/x64dbg.ini"

mklink "${HOME}/.ghidra/" "${PSScriptRoot}/own/ghidra/"
